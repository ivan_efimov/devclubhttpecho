package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httputil"
	"strconv"
)

type Entry struct {
	Key   string
	Value string
}
type HttpInfo struct {
	RawRequest      string
	ResponseHeaders []Entry
	Required        []Entry
	Headers         []Entry
}

func printStats(w http.ResponseWriter, s Stats) {
	_, _ = fmt.Fprint(w, "<p>Echo: "+strconv.Itoa(s.EchoRequestCount)+"</p>")
	_, _ = fmt.Fprint(w, "<p>Stats: "+strconv.Itoa(s.StatsRequestCount)+"</p>")
	_, _ = fmt.Fprint(w, "<p>Cookie: "+strconv.Itoa(s.SetcookieRequestCount)+"</p>")
}

func writeContents(w http.ResponseWriter, r *http.Request) {
	info := &HttpInfo{}

	rawReq, _ := httputil.DumpRequest(r, true)
	info.RawRequest = string(rawReq)

	info.Required = make([]Entry, 0)
	info.Required = append(info.Required, Entry{Key: "Method", Value: r.Method})
	info.Required = append(info.Required, Entry{Key: "Resource URI", Value: r.RequestURI})
	info.Required = append(info.Required, Entry{Key: "Protocol", Value: r.Proto})

	info.Headers = make([]Entry, 0)
	info.Headers = append(info.Headers, Entry{Key: "Host", Value: r.Host})
	for key := range r.Header {
		info.Headers = append(info.Headers, Entry{Key: key, Value: r.Header.Get(key)})
	}

	h := w.Header()
	info.ResponseHeaders = make([]Entry, 0)
	for key := range h {
		info.ResponseHeaders = append(info.ResponseHeaders, Entry{Key: key, Value: h.Get(key)})
	}

	_ = mainPage.Execute(w, info)
}

func handleEcho(w http.ResponseWriter, r *http.Request) {
	stats.EchoRequestCount++

	writeContents(w, r)
	w.WriteHeader(http.StatusOK)
}

func handleSetCookie(w http.ResponseWriter, r *http.Request) {
	stats.SetcookieRequestCount++

	http.SetCookie(w, &http.Cookie{
		Name:   cookieName + strconv.Itoa(rand.Intn(maxId)),
		Value:  "a_" + strconv.Itoa(rand.Int()) + "_" + strconv.Itoa(rand.Int()),
		MaxAge: 60, Path: "/"},
	)
	writeContents(w, r)
	w.WriteHeader(http.StatusOK)
}

func handleStats(w http.ResponseWriter, r *http.Request) {
	stats.StatsRequestCount++

	printStats(w, stats)
	w.WriteHeader(http.StatusOK)
}
func handleResetStats(w http.ResponseWriter, r *http.Request) {
	stats = Stats{}

	printStats(w, stats)
	w.WriteHeader(http.StatusOK)
}
