package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

var mainPage *template.Template

const cookieName = "id"
const maxId int = 3

type Stats struct {
	EchoRequestCount      int
	StatsRequestCount     int
	SetcookieRequestCount int
}

var stats Stats

func main() {
	var addr = flag.String("addr", "127.0.0.1", "Listen address")
	var port = flag.Int("port", 80, "Listen port")
	flag.Parse()

	mainPage, _ = template.ParseFiles("template.html")

	http.HandleFunc("/setcookie", handleSetCookie)
	http.HandleFunc("/stats", handleStats)
	http.HandleFunc("/resetstats", handleResetStats)
	http.HandleFunc("/", handleEcho)
	log.Fatal(http.ListenAndServe(*addr+":"+strconv.Itoa(*port), nil))
}
